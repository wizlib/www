<img src="logo.png" width=200 style="display: block; margin-left: auto; margin-right: auto;"/>

<h1 style="text-align: center;">The Steampunk Wizard project</h1>

<p style="text-align:center;">Command-line personal productivity tools</p>

<hr/>

<table style="border: 0 !important; border-collapse: collapse;">

<tr style="background: none;">
<td style="vertical-align:top;padding-top:20pt;">
<img src="particles.png"/>
</td>
<td style="padding-bottom:24pt;padding-top:20pt;">
<h2 style="margin:0px;">Kwark</h2>
Tap into AI brilliance from a simple shell command<br/>
<a href="https://pypi.org/project/kwark/">Package</a> &nbsp; <a href="https://gitlab.com/steamwiz/kwark">Source</a>
</td>
</tr>


<tr style="background: none;">
<td style="vertical-align:top">
<img src="monkey.png"/>
</td>
<td style="padding-bottom:24pt;">
<h2 style="margin:0px;">RepoZee</h2>
Explore the contents of a Git repository in a local filesystem with AI<br/>
<a href="https://pypi.org/project/repozee/">Package</a> &nbsp; <a href="https://gitlab.com/steamwiz/repozee">Source</a>
</td>
</tr>

<tr style="background: none;">
<td style="vertical-align:top">
<img src="bee.png"/>
</td>
<td style="padding-bottom:24pt;">
<h2 style="margin:0px;">Busy</h2>
Escape from overwhelm and stay focused all day<br/>
<a href="https://pypi.org/project/busy/">Package</a> &nbsp; <a href="https://gitlab.com/steamwiz/busy">Source</a>  &nbsp; <a href="https://busy.steamwiz.io">Docs</a>
</td>
</tr>

<tr style="background: none;">
<td style="vertical-align:top;">
<img src="elephant.png"/>
</td>
<td style="padding-bottom:24pt;">
<h2 style="margin:0px;">Filez4Eva</h2>
Rename and stow files with structured patterns<br/>
<a href="https://pypi.org/project/filez4eva/">Package</a> &nbsp; <a href="https://gitlab.com/steamwiz/filez4eva">Source</a>
</td>
</tr>

<tr style="background: none;">
<td style="vertical-align:top;">
<img src="rabbit.png"/>
</td>
<td style="padding-bottom:24pt;">
<h2 style="margin:0px;">Swytchit</h2>
Switch environment context easily in the shell<br/>
<a href="https://pypi.org/project/swytchit/">Package</a> &nbsp; <a href="https://gitlab.com/steamwiz/swytchit">Source</a>
</td>
</tr>

<tr style="background: none;">
<td style="vertical-align:top;">
<img src="rat.png"/>
</td>
<td style="padding-bottom:24pt;">
<h2 style="margin:0px;">VerNum</h2>
Generate and process semantic version numbers<br/>
<a href="https://pypi.org/project/vernum/">Package</a> &nbsp; <a href="https://gitlab.com/steamwiz/vernum">Source</a>
</td>
</tr>

<tr style="background: none;">
<td style="vertical-align:top;">
<img src="wizard.png"/>
</td>
<td style="padding-bottom:24pt;">
<h2 style="margin:0px;">WizLib</h2>
Build configurable CLI tools easily in Python (a framework)<br/>
 <a href="https://pypi.org/project/wizlib/">Package</a> &nbsp; <a href="https://gitlab.com/steamwiz/wizlib">Source</a>  &nbsp; <a href="https://wizlib.steamwiz.io">Docs</a>
</td>
</tr>

</table>

## History

The SteamPunk Wizard project emerged from a difficult period in my life. Unemployed and recovering from a major health condition, I threw myself into one activity that soothed me: coding in Python. Because of a vision impairment (and personal preference) I focused on simple, easy-to-use command-line applications to help with daily life such as a todo list manager and a utility for organizing files in Dropbox. Today, the Steampunk Wizard tools help make life go a little more smoothly, while still providing an enjoyable coding experience through weekly improvements.

Lately I've developed small tools using LLM technology, to enhance the capabilities of the collection and hone my skills in applied AI engineering.

<img src="steampunk-wizard.png" style="padding:32pt;display: block; margin-left: auto; margin-right: auto;width:320px;"/>

<div style="text-align:center;">

Built and tested on <a href="https://gitlab.com/steamwiz" target="_blank">GitLab.com</a>. Icons by <a href="https://www.flaticon.com/" target="_blank">Freepik-Flaticon</a>.

Proudly developed in <img src="Flag_of_Canada.svg" height=12pt/> Canada by <a href="https://fpotter.com" target="_blank">Francis Potter</a>.

Join us on <a href="https://communityinviter.com/apps/steamwiz/slack" target="_blank">Slack</a>

</div>
